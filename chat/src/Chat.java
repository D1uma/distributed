import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Chat extends Remote {
	//register a new user to our chat server
	public boolean register(User usr) throws RemoteException;
	
	//send  message to the server
	public boolean send(String receiver, Message msg) throws RemoteException;
	
	//polls messages from the server
	public List<Message> receive(String usr) throws RemoteException;
	
	// get the list of users connected
	public List<String> getUserList() throws RemoteException;
}
