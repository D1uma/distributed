import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ChatServer {
	public static void main(String [] args){
		  try {
		    // Create a chat remote object
			ChatImpl chatserver = new ChatImpl();
			Chat chat_stub = (Chat) UnicastRemoteObject.exportObject(chatserver, 0);
		    // Register the remote object in RMI registry with a given identifier
		    Registry registry= LocateRegistry.createRegistry(1099);
		    registry.bind("ChatServer", chat_stub);
		    System.out.println ("Chat server ready");
		  } catch (Exception e) {
		    System.err.println("Error on server :" + e) ; e.printStackTrace();
		  }
	}
}
